package com.sharma.Controller;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sharma.model.Time;


public interface TimeService {

    public void deleteById(int id);
    public void getAll();
	public Time save(String input);

}
