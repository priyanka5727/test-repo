package com.sharma.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sharma.model.Constants;
import com.sharma.model.Time;
import com.sharma.model.Utility;
import com.sharma.repository.TimeRepository;

@RestController
@ComponentScan
public class TimeController implements TimeService{

	@Autowired
	TimeRepository timeRepository;

	@Autowired 
	Utility utility;
	
	@Override
	@RequestMapping(value = "/timeSaved", method = RequestMethod.GET)
	public Time save(String input) {
		Time time = new Time();
		try {
			System.out.println(input);

			input= input.replace(Constants.MAKES, Constants.SERVES);

			System.out.println(input);
			boolean A=input.contains(Constants.ACTIVE);
			boolean T=input.contains(Constants.TOTAL);
			boolean S=input.contains(Constants.SERVES);
			String timeList=new String();
			
			if(A)
			{
				if(T)
				{
					timeList= input.substring(0,input.indexOf(Constants.TOTAL));
					time=utility.set(timeList,time);
					System.out.println(timeList);
					if(S)
					{
						timeList=input.substring(input.lastIndexOf(Constants.TOTAL),input.indexOf(Constants.SERVES));
						time=utility.set(timeList,time);
						System.out.println(timeList);
						timeList=input.substring(input.indexOf(Constants.SERVES));
						time=utility.set(timeList,time);
						System.out.println(timeList);
					}
					else
					{
						timeList=input.substring(input.indexOf(Constants.TOTAL));
						time=utility.set(timeList,time);
						System.out.println(timeList);
						
					}
				}
				else if(S)
				{
					timeList= input.substring(0,input.indexOf(Constants.SERVES));
					time=utility.set(timeList,time);
					System.out.println(timeList);
					timeList=input.substring(input.indexOf(Constants.SERVES));
					System.out.println(timeList);

					time=utility.set(timeList,time);
					System.out.println(timeList);
				}
				else
				{
					time=utility.set(input,time);
					System.out.println(input);

				}
				
			}
			else if(T)
			{
				if(S)
				{
					timeList=input.substring(0,input.indexOf(Constants.SERVES));
					time=utility.set(timeList,time);
					System.out.println(timeList);
					timeList=input.substring(input.indexOf(Constants.SERVES));
					time=utility.set(timeList,time);
					System.out.println(timeList);
				}
				else
				{
					time=utility.set(input,time);
					System.out.println(input);

				}

			}
			else if(S)
			{
				time=utility.set(input,time);
				System.out.println(input);

			}
			else
				System.out.println("no input");
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		timeRepository.save(time);
		System.out.println(timeRepository.save(time));
		return time;
	}


	@Override
	@DeleteMapping("/time/{id}")
	public void deleteById(@PathVariable int id) {
		timeRepository.deleteById(id);
	}

	@Override
	@RequestMapping(value = "/time", method = RequestMethod.GET)
	public void getAll() {
		List<Time> values = timeRepository.findAll();
		System.out.println(values);
		System.out.println("Database size......." + values.size());
	}
	
}
