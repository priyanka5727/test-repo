package com.sharma.TimeSpring;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.sharma.Controller.TimeController;
import com.sharma.model.Time;

@SpringBootApplication
@ComponentScan("com.sharma")
@EnableJpaRepositories("com.sharma.repository")
@EntityScan("com.sharma.model")
public class TimeSpringApplication {

	@Autowired(required=true)
	TimeController timecontroller;
	
	@PostConstruct
	public void run() throws Exception
	{
		
		Time output= timecontroller.save("ACTIVE: 30 hr 40 min ; TOTAL: 21 hr 10 min ; SERVES: 4");
		System.out.println("Output............"+output);
		timecontroller.getAll();
	}
	public static void main(String[] args) {
		SpringApplication.run(TimeSpringApplication.class, args);
		System.out.println("done..........");
	}

}
