package com.sharma.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

@Component
public class Utility  {

	public Time set(String ip,Time time)
	{
		try
		{
			
		ip=ip.trim().replace(Constants.ReplacePattern,"");
		System.out.println(ip);
		List<String> timeDetails = spliter(ip, Constants.SplitPattern);
		System.out.println(timeDetails);
		
		System.out.println(timeDetails.get(0));
		List<String> timeType = spliter(timeDetails.get(1), Constants.DigitPattern);
		System.out.println(timeType);

		int item = timeType.size();
		String tm = Constants.formatInitially;
		switch (item) {
		case 1: {
			List<String> timeType1 = spliter(timeDetails.get(1).trim(), Constants.NondigitPattern);
			System.out.println(timeType1);
			if(timeType1.isEmpty())
						break;
			else if(timeType1.get(0).trim().equals(Constants.hours))
					tm=update(Integer.parseInt(timeType.get(0)),0);
			else if(timeType1.get(0).trim().equals(Constants.halfHour))
				tm=update(Integer.parseInt(timeType.get(0)),30);
			else 
				tm=update(Integer.parseInt(timeType.get(0)));
			break;
		}
		case 2: {
			tm=update(Integer.parseInt(timeType.get(0)),
					Integer.parseInt(timeType.get(1)));
			System.out.println(tm);
			break;
		}
		default:
			System.out.println("something went wrong");
		}

		if (timeDetails.get(0).trim().equals(Constants.ACTIVE)) {
			System.out.println(tm);
			time.setActive(tm);
		} else if (timeDetails.get(0).trim()
				.equals(Constants.TOTAL)) {
			System.out.println(tm);
			time.setTotal(tm);
		} else if (timeDetails.get(0).trim()
				.equals(Constants.SERVES))
			time.setServes(timeDetails.get(1));
		else
			System.out.println("kuch toh loacha hai");

	} catch (Exception e) {
		System.out.println(e);
	}
		return time;

		
	}
	
	public List<String> spliter(String input, String pattern) {
		List<String> splitList = new ArrayList<String>();
		Pattern regExPattern = Pattern.compile(pattern);
		splitList = regExPattern.splitAsStream(input).filter(e -> !e.isEmpty()).collect(Collectors.toList());
		return splitList;

	}
	
	public static String toStr(long hrs, long mins)
	{
		return "00:" + hrs + ":" + mins;
	}


	public String update(int min) {
		LocalDateTime now = LocalDateTime.now();

		LocalDateTime plusMins = now.plus(min, ChronoUnit.MINUTES);

		long days = ChronoUnit.DAYS.between(now, plusMins);
		long hrs = ChronoUnit.HOURS.between(now.plus(days, ChronoUnit.DAYS),
				plusMins);
		long mins = ChronoUnit.MINUTES.between(now.plus(days, ChronoUnit.DAYS)
				.plus(hrs, ChronoUnit.HOURS), plusMins);

		DateTimeFormatter format = DateTimeFormatter.ofPattern(Constants.Format);
		if (days == 0) {
			return toStr(hrs,mins);
		} else {
			LocalDateTime DateTime = LocalDateTime.of(2000, 2, (int) days,
					(int) hrs, (int) mins, 00);
			String datetime = DateTime.format(format);
			return datetime;
		}

	}

	
	public String update(int hr, int min) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter format = DateTimeFormatter.ofPattern(Constants.Format);

		LocalDateTime plusHrs = now.plus(hr, ChronoUnit.HOURS);
				
		long days = ChronoUnit.DAYS.between(now, plusHrs);
		long hrs = ChronoUnit.HOURS.between(now.plus(days, ChronoUnit.DAYS),
				plusHrs);
		long mins = ChronoUnit.MINUTES.between(now.plus(days, ChronoUnit.DAYS)
				.plus(hrs, ChronoUnit.HOURS), plusHrs)
				+ min;
		if (days == 0) {
			return toStr(hrs, mins);
		} else {
			LocalDateTime DateTime = LocalDateTime.of(2000, 2, (int) days,
					(int) hrs, (int) mins, 00);
			String datetime = DateTime.format(format);
			return datetime;
		}
	}
}
