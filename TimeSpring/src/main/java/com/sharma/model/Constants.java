package com.sharma.model;

public class Constants {

	public static  final String ACTIVE="ACTIVE";

	public static final String TOTAL="TOTAL";

	public static final String SERVES="SERVES";

	public static final String MAKES="MAKES";

	public static final String ReplacePattern="[;]";

	public static final String SplitPattern="[:]";

	public static final String DigitPattern="\\D";

	public static final String NondigitPattern="\\d";

	public static final String hours="hr";

	public static final String halfHour="½ hr";
	
	public static final String formatInitially="00:00:00";
	
	public static final String Format="dd:hh:mm";

}
