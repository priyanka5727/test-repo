package com.sharma.TimeSpring;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.sharma.Controller.TimeController;
import com.sharma.model.Time;
import com.sharma.repository.TimeRepository;



public class TimeSpringTest {

	@Mock
	private TimeRepository timeRepository;
	
	@InjectMocks
	private TimeController timecontroller = new TimeController();
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	
	
	@Test
	public void saveTest()
	{
		Time timeResponse = new Time();
		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		String input ="";
		Time t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals(null, t.getActive());
		assertEquals(null, t.getTotal());
		assertEquals(null, t.getServes());
		
		
		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="ACTIVE: 30 hr 40 min ; TOTAL: 21 hr 10 min ; SERVES: 4";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals("01:06:40", t.getActive());
		assertEquals("00:21:10", t.getTotal());
		assertEquals(" 4", t.getServes());
		
		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="ACTIVE: 30 hr TOTAL:10 min SERVES: 4 to 6";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals("01:06:00", t.getActive());
		assertEquals("00:0:10", t.getTotal());
		assertEquals(" 4 to 6", t.getServes());
		
		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="ACTIVE: 30 hr TOTAL:10 min SERVES: 4 to 6";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals("01:06:00", t.getActive());
		assertEquals("00:0:10", t.getTotal());
		assertEquals(" 4 to 6", t.getServes());

		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="ACTIVE: 1 hr ;TOTAL: 2½ hr MAKES: 4 to 6";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals("00:1:0", t.getActive());
		assertEquals("00:2:30", t.getTotal());
		assertEquals(" 4 to 6", t.getServes());

		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="ACTIVE: 8 min TOTAL: 10 min";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals("00:0:8", t.getActive());
		assertEquals("00:0:10", t.getTotal());
		assertEquals(null, t.getServes());

		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="ACTIVE: 8 min TOTAL: 10 min";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals("00:0:8", t.getActive());
		assertEquals("00:0:10", t.getTotal());
		assertEquals(null, t.getServes());

		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="ACTIVE: 8 min TOTAL: 10 min";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals("00:0:8", t.getActive());
		assertEquals("00:0:10", t.getTotal());
		assertEquals(null, t.getServes());

		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="ACTIVE: 3hr; SERVES: 4";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals("00:3:0", t.getActive());
		assertEquals(null, t.getTotal());
		assertEquals(" 4", t.getServes());

		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="TOTAL: 2½ hr; SERVES: 4";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals(null, t.getActive());
		assertEquals("00:2:30", t.getTotal());
		assertEquals(" 4", t.getServes());
		
		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="ACTIVE: 8 min ";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals("00:0:8", t.getActive());
		assertEquals(null, t.getTotal());
		assertEquals(null, t.getServes());
		
		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="TOTAL: 10 min";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals(null, t.getActive());
		assertEquals("00:0:10", t.getTotal());
		assertEquals(null, t.getServes());
		

		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="TOTAL: 10 hr";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals(null, t.getActive());
		assertEquals("00:10:0", t.getTotal());
		assertEquals(null, t.getServes());
		
		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input=" MAKES: 4 to 6";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals(null, t.getActive());
		assertEquals(null, t.getTotal());
		assertEquals(" 4 to 6", t.getServes());
		
		Mockito.when(timeRepository.save(Mockito.any())).thenReturn(timeResponse);
		 input="ACTIVE: TOTAL:10 min SERVES: 4 to 6";
		 t= timecontroller.save(input);
		System.out.println("output...."+t);
		assertEquals(null, t.getActive());
		assertEquals(null, t.getTotal());
		assertEquals(" 4 to 6", t.getServes());
	}
	
	@Test(expected = Exception.class)
	public void getAllTest() {
		
		Mockito.doNothing().when(timeRepository).findAll();   
		timecontroller.getAll();
		Mockito.verify(timeRepository).findAll();
	}
	
	@Test(expected = Exception.class)
	public void deleteById() {
		
		Mockito.doNothing().when(timeRepository).deleteById(30);   
		timecontroller.deleteById(30);
		   Mockito.verify(timeRepository).deleteById(30);
	}
	
	@Test
	public void test() {
		System.out.println("This test Ran");
		Assert.assertTrue(true);
		
}
}
